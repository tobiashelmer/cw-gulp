const cw = (cw || {});

(($) => {

    /**
     * Main function
     */
    function Main() {

        const self = this;

        this.init = () => {
            self.smoothScroll();
        };

        this.smoothScroll = () => {
            $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click((event) => {
                if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                    let target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, () => {
                            let $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) {
                                return false;
                            } else {
                                $target.attr('tabindex', '-1');
                                $target.focus();
                            }
                        });
                    }
                }
            });
        };

    }

    $(document).ready(() => {
        cw.main = new Main();
        cw.main.init();
    });

})(jQuery);