"use strict";

const cw = (cw || {});

(($) => {

    /**
     * Hides the header on scroll-down, shows it on scroll-up
     */
    function HeaderScrollWatcher(element) {
        let self = {};

        let $element = $(element);

        self.didScroll = false;
        self.lastScrollTop = 0;
        self.delta = 4;
        self.intervalTimer = 10;
        self.navbarHeight = 118;

        self.hasScrolled = hasScrolled;
        self.debounceCheck = debounceCheck;
        self.init = init;
        self.registerEvents = registerEvents;
        self.initVars = initVars;

        if (!cw.editmode) {
            self.init();
        }

        return self;

        function init() {
            self.initVars();
            self.registerEvents();
        }


        function registerEvents() {
            $(window).scroll((e) => {
                self.didScroll = true;
            });
            self.interval = setInterval(debounceCheck, self.intervalTimer);
        }

        function initVars() {
            self.navbarHeight = $element.outerHeight();
        }

        function hasScrolled() {
            let st = $(document).scrollTop();
            if (st < self.navbarHeight) {
                $element.removeClass('animated');
            } else {
                $element.addClass('animated');
            }
            if (Math.abs(self.lastScrollTop - st) <= self.delta) {
                return;
            }
            // If current position > last position AND scrolled past navbar...
            if (st > self.lastScrollTop && st > self.navbarHeight) {
                // Scroll Down
                $element.removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                // If did not scroll past the document (possible on mac)...
                if (st + $(window).height() < $(document).height()) {
                    $element.removeClass('nav-up').addClass('nav-down');
                }
            }
            self.lastScrollTop = st;
        }

        function debounceCheck() {
            if (self.didScroll) {
                self.hasScrolled();
                self.didScroll = false;
            }
        }
    }

    cw.HeaderScrollWatcher = HeaderScrollWatcher;

    $(document).ready(() => {
        $('.smart-hide').each((index, element) => {
            let $elem = $(element);
            let data = $elem.data('cw.HeaderScrollWatcher');
            if (!data) {
                $elem.data('cw.HeaderScrollWatcher', new HeaderScrollWatcher($elem));
            }
        });
        $('.burger').on('click', () => {
            $(this).toggleClass('is-active');
            if (!$(this).hasClass('is-active')) {
                $(this).addClass('not-active');
            } else {
                $(this).removeClass('not-active');
            }
            $('header').toggleClass('open');
        })
    });

})(jQuery);