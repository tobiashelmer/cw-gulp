/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.toolbarGroups = [
        // { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] }//,
        // { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        // { name: 'links', groups: [ 'links' ] },
        // { name: 'insert', groups: [ 'insert' ] },
        // { name: 'forms', groups: [ 'forms' ] },
        // { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        // { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        // { name: 'tools', groups: [ 'tools' ] },
        // { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        // { name: 'others', groups: [ 'others' ] },
        // '/',
        // { name: 'styles', groups: [ 'styles' ] },
        // { name: 'colors', groups: [ 'colors' ] },
        // { name: 'about', groups: [ 'about' ] }
    ];

    //config.removeButtons = 'Underline,Subscript,Scayt,Image,Table,HorizontalRule,SpecialChar,Maximize,Strike,RemoveFormat,Outdent,Indent';
};

CKEDITOR.stylesSet.add( 'default', [
    { name: 'Button', element: 'a', attributes: { 'class': 'btn btn-primary' } }
]);