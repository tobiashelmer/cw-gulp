'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import install from 'gulp-install';
import imagemin from 'gulp-imagemin';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import del from 'del';


/**
 * Configuration variables
 *
 * add important libraries which need to load first in path.source.js.libs.head
 * add new node_modues and external libraries in path.source.js.libs.bottom
 *
 */
const version = '1.0.0';
const config = {
    url: '',
    path: {
        package: './package.json',
        watch: [
            '../../app/Resources/views/**/*.html.php',
            '../../src/AppBundle/Controller/**/*.php'
        ],
        source: {
            js: {
                libs: {
                    head: [
                        './node_modules/jquery/dist/jquery.js',
                        './js/libs/svgxuse.js'
                    ],
                    bottom: [
                        './node_modules/popper.js/dist/umd/popper.js',
                        './node_modules/bootstrap/js/dist/util.js',
                        './node_modules/bootstrap/js/dist/collapse.js',
                        './node_modules/bootstrap/js/dist/modal.js',
                        './node_modules/bootstrap/js/dist/dropdown.js',
                    ]
                },
                main: [
                    './js/init/*.js',
                    './js/*.js'
                ],
                prod: [
                    './dist/js/main.js',
                    './dist/js/libs-bottom.js'
                ]
            },
            sass: './sass/**/*.scss',
            css: [
                './dist/css/libs.min.css',
                './dist/css/main.min.css'
            ],
            images: [
                './img/**/*.png',
                './img/**/*.jpg',
                './img/**/*.jpeg',
                './img/**/*.gif',
                './img/**/*.svg'
            ]
        },
        dist: {
            js: './dist/js/',
            css: './dist/css/',
            img: './dist/img/'
        }
    },
    filename: {
        js: {
            main: 'main.js',
            prod: 'cw-prod-' + version + '.js',
            head: 'libs-head-' + version + '.js',
            bottom: 'libs-bottom.js'
        },
        css: {
            prod: 'cw-prod-' + version + '.css',
        },
        suffix: '.min'
    },
    sass: {
        errorLogToConsole: true,
        outputStyle: 'compressed'
    },
    prefixerOptions: {
        browsers: ['last 10 versions']
    }
};

/**
 * runs npm install
 */
gulp.task('npm install', () => {
    gulp.src(config.path.package)
        .pipe(install());
});

/**
 * Task for css files
 * uses autoprefixer for browser-prefixes and sourcemaps - compile everything to .css
 */
gulp.task('sass', () => {
    return gulp.src(config.path.source.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(config.prefixerOptions))
        .pipe(sourcemaps.write('.'))
        .pipe(browserSync.stream())
        .pipe(gulp.dest(config.path.dist.css));
});

/**
 * Task for .min.css files
 * uses autoprefixer for browser-prefixes and sourcemaps - compile everything to .css
 */
gulp.task('sass:minify', () => {
    return gulp.src(config.path.source.sass)
        .pipe(sourcemaps.init())
        .pipe(sass(config.sass).on('error', sass.logError))
        .pipe(rename({suffix: config.filename.suffix}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.path.dist.css));
});

/**
 * Watch Task for sass - executes sass task upon getting called
 */
gulp.task('sass:watch', () => {
    gulp.watch('sass/**/*.scss', ['sass']);
});

/**
 * Task for main.js - uses Babel to transpile ES6 code to usuable JS code for older browsers
 */
gulp.task('js', ['js:libs-head', 'js:libs-bottom'], () => {
    return gulp.src(config.path.source.js.main)
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat(config.filename.js.main))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.path.dist.js));
});

/**
 * Watch Task for js - executes js task upon getting called
 */
gulp.task('js:watch', () => {
    gulp.watch('js/**/*.js', ['js']);
});

/**
 * Task for libs-head.min.js - concat files and outputs minified version
 */
gulp.task('js:libs-head', () => {
    return gulp.src(config.path.source.js.libs.head)
        .pipe(sourcemaps.init())
        .pipe(concat(config.filename.js.head))
        .pipe(uglify())
        .pipe(rename({
            suffix: config.filename.suffix
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.path.dist.js));
});

/**
 * Task for libs-bottom.min.js - concat files and outputs normal version
 */
gulp.task('js:libs-bottom', () => {
    return gulp.src(config.path.source.js.libs.bottom)
        .pipe(sourcemaps.init())
        .pipe(concat(config.filename.js.bottom))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.path.dist.js))
});

/**
 * Concat Tasks for SASS and JavaScript Files
 */
gulp.task('concat:js', () => {
    return gulp.src(config.path.source.js.prod)
        .pipe(concat(config.filename.js.prod))
        .pipe(uglify())
        .pipe(rename({
            suffix: config.filename.suffix
        }))
        .pipe(gulp.dest(config.path.dist.js));
});

gulp.task('concat:sass', () => {
    return gulp.src(config.path.source.css)
        .pipe(concat(config.filename.css.prod))
        .pipe(rename({
            suffix: config.filename.suffix
        }))
        .pipe(gulp.dest(config.path.dist.css));
});

/**
 * Imagemin task to reduce size of images in static folder
 */
gulp.task('imagemin', () =>
    gulp.src(config.path.source.images)
        .pipe(imagemin())
        .pipe(gulp.dest(config.path.dist.img))
);

/**
 * Tasks for folder deletion
 */
gulp.task('clean:dist', () => {
    return del('dist/**/*');
});

gulp.task('clean:js', () => {
    return del('dist/js/*');
});

gulp.task('clean:sass', () => {
    return del('dist/sass/*');
});

/**
 * Tasks for distribution
 * dist - deletes dist folder, executes SASS and JS tasks and concatenates everything
 * dist:js - deletes dist/js folder, executes only JS tasks and concatenates the JS files
 * dist:sass - deletes dist/sass folder, executes only SASS tasks and concatenates the SASS files
 */
gulp.task('dist', (cb) => {
    runSequence(
        'clean:dist',
        ['imagemin', 'sass', 'js', 'js:libs-head', 'js:libs-bottom'],
        'sass:minify',
        ['concat:sass', 'concat:js'],
        cb);
});

gulp.task('dist:js', (cb) => {
    runSequence(
        'clean:js',
        ['js', 'js:libs-head', 'js:libs-bottom'],
        'concat:js',
        cb);
});

gulp.task('dist:sass', (cb) => {
    runSequence(
        'clean:sass',
        'sass',
        'sass:minify',
        'concat:sass',
        cb);
});

/**
 * Watch tasks
 * creates local server with browsersync and watches every change in SASS and JS files
 */
gulp.task('watch', ['sass', 'js', 'sass:watch', 'js:watch'], () => {
    browserSync.init({
        files: config.path.watch,
        proxy: config.url,
        startPath: "/"
    });
});